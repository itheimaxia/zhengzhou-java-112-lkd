package com.lkd.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lkd.entity.ChannelEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ChannelDao extends BaseMapper<ChannelEntity> {

    @Select("select * from tb_channel where inner_code=#{innerCode}")
    @Results(id="channelMap",value = {
            @Result(property = "channelId",column = "channel_id"),
            @Result(property = "skuId",column = "sku_id"),
            @Result(property = "sku",column = "sku_id",one = @One(select = "com.lkd.dao.SkuDao.getById"))
    })
    List<ChannelEntity> getChannelsByInnerCode(String innerCode);


    @Select("SELECT tc.*\n" +
            "from tb_channel tc inner join tb_vending_machine tvm on tc.inner_code = tvm.inner_code \n" +
            "where tvm.vm_status = 1 and tc.max_capacity > tc.current_capacity and tvm.id % #{total} = #{index} ")
    @Results(id="channelMap2",value = {
            @Result(property = "channelId",column = "channel_id"),
            @Result(property = "skuId",column = "sku_id"),
            @Result(property = "sku",column = "sku_id",one = @One(select = "com.lkd.dao.SkuDao.getById"))
    })
    List<ChannelEntity> getChannelsNeedToSupply(int total,int index );
}
