package com.lkd.service.impl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Strings;
import com.lkd.dao.NodeDao;
import com.lkd.entity.NodeEntity;
import com.lkd.entity.VendingMachineEntity;
import com.lkd.service.NodeService;
import com.lkd.service.VendingMachineService;
import com.lkd.vo.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NodeServiceImpl  extends ServiceImpl<NodeDao, NodeEntity> implements NodeService{

    @Autowired
    private NodeDao nodeDao;
    @Override
    public Pager<NodeEntity> search(String name, String regionId, long pageIndex, long pageSize) {

        Page<NodeEntity> page =  new Page<>(pageIndex,pageSize);
        LambdaQueryWrapper<NodeEntity> queryWrapper = new LambdaQueryWrapper<>();
        //where name = '张三'
        //Wrappers.query().eq("nick_name","张三");
        //update xxx set a = a + 1
        //Wrappers.update().setSql("a = a + 1").lambda()

        if(!Strings.isNullOrEmpty(name)){
            queryWrapper.like(NodeEntity::getName,name);
        }
        if(!Strings.isNullOrEmpty(regionId)){
            Long regionIdLong = Long.valueOf(regionId);
            queryWrapper.eq(NodeEntity::getRegionId,regionIdLong);
        }
        //PageResponseResult -> code msg total list page size
        return Pager.build(this.page(page,queryWrapper));
    }

    @Autowired
    private VendingMachineService vmService;

    @Override
    public List<VendingMachineEntity> getVmList(long id) {
        QueryWrapper<VendingMachineEntity> qw = new QueryWrapper<>();
        qw.lambda()
                .eq(VendingMachineEntity::getNodeId,id);

        return vmService.list(qw);
    }




}
