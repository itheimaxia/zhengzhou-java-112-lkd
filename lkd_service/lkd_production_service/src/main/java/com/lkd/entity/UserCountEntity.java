package com.lkd.entity;

import lombok.Data;

/**
 * @author brianxia
 * @version 1.0
 * @date 2022-07-19 15:02
 */
@Data
public class UserCountEntity {
    private Long count;
    private Integer userId;
}
