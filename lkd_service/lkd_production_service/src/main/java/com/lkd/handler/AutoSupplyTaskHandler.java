package com.lkd.handler;

import com.lkd.business.MsgHandler;
import com.lkd.common.VMSystem;
import com.lkd.config.TopicConfig;
import com.lkd.contract.StatusInfo;
import com.lkd.contract.SupplyChannel;
import com.lkd.contract.SupplyContract;
import com.lkd.contract.VmStatusContract;
import com.lkd.emq.Topic;
import com.lkd.feign.VMService;
import com.lkd.http.vo.TaskDetailsViewModel;
import com.lkd.http.vo.TaskViewModel;
import com.lkd.service.TaskService;
import com.lkd.utils.JsonUtil;
import com.lkd.vo.VmVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author brianxia
 * @version 1.0
 * @date 2023-02-10 16:19
 */
@Component
@Topic(TopicConfig.TASK_SUPPLY_AUTOTOPIC)
@Slf4j
public class AutoSupplyTaskHandler implements MsgHandler {

    @Autowired
    private TaskService taskService;
    @Autowired
    private VMService vmService;

    @Override
    public void process(String jsonMsg) throws IOException {
        //1.消息转换成对象
        if(StringUtils.isBlank(jsonMsg)){
            log.error("消息为空" + TopicConfig.TASK_SUPPLY_AUTOTOPIC);
            return;
        }

        //2.实体类转换json
        SupplyContract supplyContract = JsonUtil.getByJson(jsonMsg, SupplyContract.class);
        String innerCode = supplyContract.getInnerCode();
        List<SupplyChannel> supplyChannels = supplyContract.getSupplyData();
        if(StringUtils.isBlank(innerCode) || CollectionUtils.isEmpty(supplyChannels)){
            log.error("参数错误!" + jsonMsg + " topic:" + TopicConfig.VMS_STATUS_TOPIC);
            return;
        }

        //3.创建自动补货工单
        TaskViewModel taskViewModel = new TaskViewModel();
        taskViewModel.setInnerCode(innerCode);
        taskViewModel.setAssignorId(0);
        VmVO vmInfo = vmService.getVMInfo(innerCode);
        if(vmInfo == null){
            log.error("售货机编码错误" + innerCode);
            return;
        }
        Integer leastTaskUser = taskService.getLeastTaskUser(vmInfo.getRegionId(), false);
        if(leastTaskUser == null){
            log.error("redis中不存在维修人员" + vmInfo.getRegionId());
            return;
        }
        taskViewModel.setUserId(leastTaskUser);
        taskViewModel.setProductType(VMSystem.TASK_TYPE_SUPPLY);
        taskViewModel.setCreateType(VMSystem.TASK_CREATETYPE_AUTO);

        //工单详情
        List<TaskDetailsViewModel> list = supplyChannels.stream().map(x -> {
            TaskDetailsViewModel taskDetailsViewModel = new TaskDetailsViewModel();
            BeanUtils.copyProperties(x, taskDetailsViewModel);
            taskDetailsViewModel.setExpectCapacity(x.getCapacity());
            return taskDetailsViewModel;
        }).collect(Collectors.toList());

        taskViewModel.setDetails(list);

        taskService.create(taskViewModel);
    }
}
