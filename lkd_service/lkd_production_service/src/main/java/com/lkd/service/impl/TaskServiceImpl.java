package com.lkd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import com.lkd.common.VMSystem;
import com.lkd.config.TopicConfig;
import com.lkd.contract.SupplyChannel;
import com.lkd.contract.SupplyContract;
import com.lkd.contract.TaskCompleteContract;
import com.lkd.dao.TaskDao;
import com.lkd.dao.TaskDaoXml;
import com.lkd.emq.MqttProducer;
import com.lkd.entity.TaskCollectEntity;
import com.lkd.entity.TaskDetailsEntity;
import com.lkd.entity.TaskEntity;
import com.lkd.entity.TaskStatusTypeEntity;
import com.lkd.exception.LogicException;
import com.lkd.feign.UserService;
import com.lkd.feign.VMService;
import com.lkd.http.vo.CancelTaskViewModel;
import com.lkd.http.vo.TaskDetailsViewModel;
import com.lkd.http.vo.TaskReportInfoVO;
import com.lkd.http.vo.TaskViewModel;
import com.lkd.service.TaskDetailsService;
import com.lkd.service.TaskService;
import com.lkd.service.TaskStatusTypeService;
import com.lkd.utils.UserThreadLocal;
import com.lkd.vo.Pager;
import com.lkd.vo.UserVO;
import com.lkd.vo.UserWorkVO;
import com.lkd.vo.VmVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.sql.Array;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TaskServiceImpl extends ServiceImpl<TaskDao, TaskEntity> implements TaskService {

    @Autowired
    private TaskStatusTypeService statusTypeService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private VMService vmService;

    @Autowired
    private UserService userService;

    @Autowired
    private TaskDetailsService taskDetailsService;

    @Autowired
    private MqttProducer mqttProducer;

    @Autowired
    private TaskDaoXml taskDaoXml;


    @Override
    public Pager<TaskEntity> search(Long pageIndex, Long pageSize, String innerCode, Integer userId, String taskCode, Integer status, Boolean isRepair, String start, String end) {
        Page<TaskEntity> page = new Page<>(pageIndex, pageSize);
        LambdaQueryWrapper<TaskEntity> qw = new LambdaQueryWrapper<>();
        if (!Strings.isNullOrEmpty(innerCode)) {
            qw.eq(TaskEntity::getInnerCode, innerCode);
        }
        if (userId != null && userId > 0) {
            qw.eq(TaskEntity::getUserId, userId);
        }
        if (!Strings.isNullOrEmpty(taskCode)) {
            qw.like(TaskEntity::getTaskCode, taskCode);
        }
        if (status != null && status > 0) {
            qw.eq(TaskEntity::getTaskStatus, status);
        }
        if (isRepair != null) {
            if (isRepair) {
                qw.ne(TaskEntity::getProductTypeId, VMSystem.TASK_TYPE_SUPPLY);
            } else {
                qw.eq(TaskEntity::getProductTypeId, VMSystem.TASK_TYPE_SUPPLY);
            }
        }
        if (!Strings.isNullOrEmpty(start) && !Strings.isNullOrEmpty(end)) {
            qw
                    .ge(TaskEntity::getCreateTime, LocalDate.parse(start, DateTimeFormatter.ISO_LOCAL_DATE))
                    .le(TaskEntity::getCreateTime, LocalDate.parse(end, DateTimeFormatter.ISO_LOCAL_DATE));
        }
        //根据最后更新时间倒序排序
        qw.orderByDesc(TaskEntity::getUpdateTime);

        return Pager.build(this.page(page, qw));
    }


    @Override
    public List<TaskStatusTypeEntity> getAllStatus() {
        QueryWrapper<TaskStatusTypeEntity> qw = new QueryWrapper<>();
        qw.lambda()
                .ge(TaskStatusTypeEntity::getStatusId, VMSystem.TASK_STATUS_CREATE);

        return statusTypeService.list(qw);
    }

    @Override
    @Transactional(rollbackFor = Exception.class) //spring默认只对runtimeException生效
    public boolean create(TaskViewModel taskViewModel) {

        //判断售货机 这种类型工单是否有未完成的
        String innerCode = taskViewModel.getInnerCode();
        //工单类型 维修,补货...
        int productType = taskViewModel.getProductType();
        checkSameTypeTaskIsExist(innerCode, productType);

        VmVO vmInfo = vmService.getVMInfo(taskViewModel.getInnerCode());
        if (vmInfo == null) {
            throw new LogicException("售货机编码不正确!");
        }

        //校验售货机状态
        checkVmStatus(productType, vmInfo);

        //获取执行人信息
        UserVO user = userService.getUser(taskViewModel.getUserId());
        if (user == null) {
            throw new LogicException("执行人不存在!");
        }

        //1.创建数据库实体类
        TaskEntity task = new TaskEntity();
        BeanUtils.copyProperties(taskViewModel, task);
        //字段名不一致需要手动赋值
        task.setProductTypeId(productType);
        //生成唯一编号 202302070001
        task.setTaskCode(generateTaskCode());
        //工单状态强制设置成待处理
        task.setTaskStatus(VMSystem.TASK_STATUS_CREATE);
        //设置区域id
        task.setRegionId(vmInfo.getRegionId());
        //设置执行人名称
        task.setUserName(user.getUserName());
        //设置地址
        task.setAddr(vmInfo.getNodeAddr());
        //保存到数据库
        save(task);

        if (productType == VMSystem.TASK_TYPE_SUPPLY) {
            //写详情表
            Long taskId = task.getTaskId(); //有值，回写id值
            List<TaskDetailsViewModel> details = taskViewModel.getDetails();
//        List<TaskDetailsEntity> taskDetailsEntities = new ArrayList<>();
//        for (TaskDetailsViewModel detail : details) {
//            TaskDetailsEntity taskDetailsEntity = new TaskDetailsEntity();
//            BeanUtils.copyProperties(detail,taskDetailsEntity);
//            taskDetailsEntity.setTaskId(taskId);
//            taskDetailsEntities.add(taskDetailsEntity);
//        }

            List<TaskDetailsEntity> list = details.stream().map(x -> {
                TaskDetailsEntity taskDetailsEntity = new TaskDetailsEntity();
                BeanUtils.copyProperties(x, taskDetailsEntity);
                taskDetailsEntity.setTaskId(taskId);
                return taskDetailsEntity;
            }).collect(Collectors.toList());

//            details.forEach(x -> {
//
//            });

            taskDetailsService.saveBatch(list);

        }

        //创建工单成功，给用户添加工单数
        String key = VMSystem.generateTaskCountRedisKey(user.getRoleCode(), user.getRegionId());
        redisTemplate.opsForZSet().incrementScore(key,user.getUserId(),1.0);

        return true;
    }

    @Override
    public void accept(Long taskId) {
        TaskEntity task = getById(taskId);
        //判断工单是否存在
        if (task == null) {
            throw new LogicException("工单不存在!");
        }

        if (!Objects.equals(task.getUserId(), UserThreadLocal.userId.get())) {
            throw new LogicException("工单处理人不正确!");
        }

        if (!Objects.equals(task.getTaskStatus(), VMSystem.TASK_STATUS_CREATE)) {
            throw new LogicException("订单状态不正确!");
        }

//        task.setTaskStatus(VMSystem.TASK_STATUS_PROGRESS);
//        updateById(task);
        update(Wrappers.<TaskEntity>lambdaUpdate().set(TaskEntity::getTaskStatus, VMSystem.TASK_STATUS_PROGRESS)
                .eq(TaskEntity::getTaskId, taskId)
        );
    }

    @Override
    public void cancel(Long taskId, CancelTaskViewModel vo) {
        TaskEntity task = getById(taskId);
        //判断工单是否存在
        if (task == null) {
            throw new LogicException("工单不存在!");
        }

        if (!Objects.equals(task.getUserId(), UserThreadLocal.userId.get())) {
            throw new LogicException("工单处理人不正确!");
        }

        if (Objects.equals(task.getTaskStatus(), VMSystem.TASK_STATUS_CANCEL)
                || Objects.equals(task.getTaskStatus(), VMSystem.TASK_STATUS_FINISH)) {
            throw new LogicException("订单状态不正确!");
        }

        //更新状态为取消 && 更新描述
        update(Wrappers.<TaskEntity>lambdaUpdate().set(TaskEntity::getTaskStatus, VMSystem.TASK_STATUS_CANCEL)
                .set(TaskEntity::getDesc,vo.getDesc())
                .eq(TaskEntity::getTaskId, taskId)
        );

        //获取执行人信息
        UserVO user = userService.getUser(task.getUserId());
        if (user == null) {
            throw new LogicException("执行人不存在!");
        }
        //完成工单，工单数 -1
        String key = VMSystem.generateTaskCountRedisKey(user.getRoleCode(), user.getRegionId());
        redisTemplate.opsForZSet().incrementScore(key,user.getUserId(),-1.0);
    }

    @Override
    public void complete(Long taskId) {
        TaskEntity task = getById(taskId);
        //判断工单是否存在
        if (task == null) {
            throw new LogicException("工单不存在!");
        }

        if (!Objects.equals(task.getUserId(), UserThreadLocal.userId.get())) {
            throw new LogicException("工单处理人不正确!");
        }

        if (Objects.equals(task.getTaskStatus(), VMSystem.TASK_STATUS_CANCEL)
                || Objects.equals(task.getTaskStatus(), VMSystem.TASK_STATUS_FINISH)) {
            throw new LogicException("订单状态不正确!");
        }

        //更新状态为完成 && 更新描述
        update(Wrappers.<TaskEntity>lambdaUpdate().set(TaskEntity::getTaskStatus, VMSystem.TASK_STATUS_FINISH)
                .eq(TaskEntity::getTaskId, taskId)
        );

        //添加feign同步调用，当工单是投放功能，将售货机状态修改为已运营
//        if(Objects.equals(task.getProductTypeId(),VMSystem.TASK_TYPE_DEPLOY)){
//            vmService.modifyStatus(task.getInnerCode(),true);
//        }

        //发送消息
        if(!Objects.equals(task.getProductTypeId(),VMSystem.TASK_TYPE_SUPPLY)){
            try {
                TaskCompleteContract contract = new TaskCompleteContract();
                contract.setTaskType(task.getProductTypeId());
                contract.setInnerCode(task.getInnerCode());
                mqttProducer.send(TopicConfig.VMS_COMPLETED_TOPIC,1,contract);
            } catch (JsonProcessingException e) {
                log.error("json转换错误",e);
            }
        }else{
            //补货工单
            SupplyContract supplyContract = new SupplyContract();
            supplyContract.setInnerCode(task.getInnerCode());

            //从详情表中获取数据，填充到协议中
            List<TaskDetailsEntity> list = taskDetailsService.list(Wrappers.<TaskDetailsEntity>lambdaQuery()
                    .eq(TaskDetailsEntity::getTaskId, taskId)
            );
            if(CollectionUtils.isEmpty(list)){
                log.error("工单详情表为空" + taskId);
                throw new LogicException("数据不正确!");
            }
            List<SupplyChannel> supplyChannels = list.stream().map(x -> {
                SupplyChannel supplyChannel = new SupplyChannel();
                supplyChannel.setChannelCode(x.getChannelCode());
                supplyChannel.setCapacity(x.getExpectCapacity());
                supplyChannel.setSkuId(x.getSkuId());
                supplyChannel.setSkuName(x.getSkuName());
                supplyChannel.setSkuImage(x.getSkuImage());
                return supplyChannel;
            }).collect(Collectors.toList());
            supplyContract.setSupplyData(supplyChannels);

            try {
                mqttProducer.send(TopicConfig.VMS_SUPPLY_TOPIC,2,supplyContract);
            } catch (JsonProcessingException e) {
                log.error("json转换错误");
            }
        }

        //获取执行人信息
        UserVO user = userService.getUser(task.getUserId());
        if (user == null) {
            throw new LogicException("执行人不存在!");
        }
        //完成工单，工单数 -1
        String key = VMSystem.generateTaskCountRedisKey(user.getRoleCode(), user.getRegionId());
        redisTemplate.opsForZSet().incrementScore(key,user.getUserId(),-1.0);
    }

    @Override
    public Integer getLeastTaskUser(long regionId, boolean isRepair) {
        String key = VMSystem.generateTaskCountRedisKey(isRepair?VMSystem.ROLE_CODE_REPAIRER:VMSystem.ROLE_CODE_OPERATOR,
                regionId);
        Set<Object> set = redisTemplate.opsForZSet().range(key, 0, 0);
        if(CollectionUtils.isEmpty(set)){
            return null;
        }
        return  (Integer)(new ArrayList<>(set).get(0));
    }

    @Override
    public List<TaskReportInfoVO> taskReportinfo(LocalDateTime start, LocalDateTime end) {

        //1.创建返回集合
        List<TaskReportInfoVO> taskReportInfoVOS = new ArrayList<>();

        //2.创建运营人员数据
        TaskReportInfoVO vo1 = new TaskReportInfoVO();
        taskReportInfoVOS.add(vo1);
        Integer operatorCount = userService.getOperatorCount();
        vo1.setWorkerCount(operatorCount);
        vo1.setRepair(false); //运营人员
        vo1.setTotal(countTask(start,end,false,null));
        vo1.setProgressTotal(countTask(start,end,false,VMSystem.TASK_STATUS_PROGRESS));
        vo1.setCancelTotal(countTask(start,end,false,VMSystem.TASK_STATUS_CANCEL));
        vo1.setCompletedTotal(countTask(start,end,false,VMSystem.TASK_STATUS_FINISH));

        //3.创建运维人员数据
        TaskReportInfoVO vo2 = new TaskReportInfoVO();
        taskReportInfoVOS.add(vo2);
        Integer repairerCount = userService.getRepairerCount();
        vo2.setWorkerCount(repairerCount);
        vo2.setRepair(true); //运维人员
        vo2.setTotal(countTask(start,end,true,null));
        vo2.setProgressTotal(countTask(start,end,true,VMSystem.TASK_STATUS_PROGRESS));
        vo2.setCancelTotal(countTask(start,end,true,VMSystem.TASK_STATUS_CANCEL));
        vo2.setCompletedTotal(countTask(start,end,true,VMSystem.TASK_STATUS_FINISH));

        return taskReportInfoVOS;
    }

    /**
     * 统计工单数
     * @param start 开始时间
     * @param end 结束时间
     * @param repair 是否是维修工单
     * @param taskStatus 状态 null->查询所有工单
     * @return 工单数
     */
    private int countTask(LocalDateTime start, LocalDateTime end,boolean repair,Integer taskStatus){

        LambdaQueryWrapper<TaskEntity> wrapper =
                Wrappers.<TaskEntity>lambdaQuery().between(TaskEntity::getUpdateTime, start, end);

        //运营或者运维
        if(repair){
            //运维工单  != 2
            wrapper.ne(TaskEntity::getProductTypeId,VMSystem.TASK_TYPE_SUPPLY);
        }else{
            //运营工单  == 2
            wrapper.eq(TaskEntity::getProductTypeId,VMSystem.TASK_TYPE_SUPPLY);
        }

//        if(taskStatus != null){
//            wrapper.eq(TaskEntity::getTaskStatus,taskStatus);
//        }
        wrapper.eq(taskStatus != null,TaskEntity::getTaskStatus,taskStatus);
        return count(wrapper);
    }

    @Override
    public List<UserWorkVO> userWorkTop10(LocalDate startDate, LocalDate startEnd, boolean isRepair, int regionId) {
        Page<UserWorkVO> page = new Page<>();
        page.setCurrent(1);
        page.setSize(10);
        //分页时执行两条sql 不执行selectcount(*)  select数据
        page.setSearchCount(false);
        return taskDaoXml.userWorkTop10(page,startDate,startEnd.plusDays(1),isRepair,regionId);
    }

    /**
     * 统计工单数
     * @param start 开始时间
     * @param end 结束时间
     * @param taskStatus 状态 null->查询所有工单
     * @return 工单数
     */
    private int countTask(LocalDate start, LocalDate end, Integer taskStatus){

        LambdaQueryWrapper<TaskEntity> wrapper =
                Wrappers.<TaskEntity>lambdaQuery().ge(TaskEntity::getUpdateTime, start)
                        .lt(TaskEntity::getUpdateTime, end);

//        if(taskStatus != null){
//            wrapper.eq(TaskEntity::getTaskStatus,taskStatus);
//        }
        wrapper.eq(taskStatus != null,TaskEntity::getTaskStatus,taskStatus);
        return count(wrapper);
    }


    @Override
    public List<TaskCollectEntity> collectReport(LocalDate startDate, LocalDate endDate) {
        return startDate.datesUntil(endDate.plusDays(1), Period.ofDays(1)).map(x -> {
            TaskCollectEntity entity = new TaskCollectEntity();
            entity.setCancelCount(countTask(x, x.plusDays(1), VMSystem.TASK_STATUS_CANCEL));
            entity.setFinishCount(countTask(x, x.plusDays(1), VMSystem.TASK_STATUS_FINISH));
            entity.setProgressCount(countTask(x, x.plusDays(1), VMSystem.TASK_STATUS_PROGRESS));
            entity.setCollectDate(x);
            return entity;
        }).collect(Collectors.toList());
    }

    private void checkVmStatus(int productType, VmVO vmInfo) {
        //校验售货机状态和工单类型是否匹配
        //1.投放 售货机状态为运营状态，抛出异常
        if (productType == VMSystem.TASK_TYPE_DEPLOY &&
                vmInfo.getVmStatus().equals(VMSystem.VM_STATUS_RUNNING)) {
            throw new LogicException("售货机状态为运营，无法投放!");
        }
        //2.撤机 售货机状态为非运营状态，抛出异常
        if (productType == VMSystem.TASK_TYPE_REVOKE &&
                !vmInfo.getVmStatus().equals(VMSystem.VM_STATUS_RUNNING)) {
            throw new LogicException("售货机状态为运营，无法投放!");
        }
        //3.补货 售货机状态为非运营状态，抛出异常
        if (productType == VMSystem.TASK_TYPE_SUPPLY &&
                !vmInfo.getVmStatus().equals(VMSystem.VM_STATUS_RUNNING)) {
            throw new LogicException("售货机状态为运营，无法补货!");
        }
    }

    /**
     * 校验是否有同类型未完成的工单存在
     *
     * @param innerCode   售货机编号
     * @param productType 工单类型
     */
    private void checkSameTypeTaskIsExist(String innerCode, int productType) {
        int count = count(Wrappers.<TaskEntity>lambdaQuery()
                .eq(TaskEntity::getInnerCode, innerCode)
                .eq(TaskEntity::getProductTypeId, productType)
                .in(TaskEntity::getTaskStatus, 1, 2)
        );
        if (count > 0) {
            //存在
            throw new LogicException("同类型的工单已存在，无法重复创建!");
        }
    }


    /**
     * 生成工单编号
     *
     * @return
     */
    private String generateTaskCode() {
        //日期+序号
        String date = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));  //日期字符串
        String key = "lkd.task.code." + date; //redis key
        Object obj = redisTemplate.opsForValue().get(key);
        if (obj == null) {
            redisTemplate.opsForValue().set(key, 1L, Duration.ofDays(1));
            return date + "0001";
        }
        return date + Strings.padStart(redisTemplate.opsForValue().increment(key, 1).toString(), 4, '0');
    }


}
