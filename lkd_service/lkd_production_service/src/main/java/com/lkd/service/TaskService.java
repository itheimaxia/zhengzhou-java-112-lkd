package com.lkd.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lkd.entity.TaskCollectEntity;
import com.lkd.entity.TaskEntity;
import com.lkd.entity.TaskStatusTypeEntity;
import com.lkd.exception.LogicException;
import com.lkd.http.vo.CancelTaskViewModel;
import com.lkd.http.vo.TaskReportInfoVO;
import com.lkd.http.vo.TaskViewModel;
import com.lkd.vo.Pager;
import com.lkd.vo.UserWorkVO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 工单业务逻辑
 */
public interface TaskService extends IService<TaskEntity> {


    /**
     * 通过条件搜索工单列表
     * @param pageIndex
     * @param pageSize
     * @param innerCode
     * @param userId
     * @param taskCode
     * @param isRepair 是否是运维工单
     * @return
     */
    Pager<TaskEntity> search(Long pageIndex, Long pageSize, String innerCode, Integer userId, String taskCode, Integer status, Boolean isRepair, String start, String end);





    /**
     * 获取所有状态类型
     * @return
     */
    List<TaskStatusTypeEntity> getAllStatus();


    boolean create(TaskViewModel taskViewModel);

    void accept(Long taskId);

    void cancel(Long taskId,CancelTaskViewModel vo);

    void complete(Long taskId);

    //获取最少工单数的人员
    Integer getLeastTaskUser(long regionId,boolean isRepair);

    List<TaskReportInfoVO> taskReportinfo(LocalDateTime start, LocalDateTime end);

    List<UserWorkVO> userWorkTop10(LocalDate startDate, LocalDate startEnd, boolean isRepair, int regionId);

    List<TaskCollectEntity> collectReport(LocalDate startDate, LocalDate endDate);
}
