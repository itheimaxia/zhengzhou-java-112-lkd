package com.lkd.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lkd.vo.UserWorkVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * @author brianxia
 * @version 1.0
 * @date 2023-02-12 15:56
 */
@Mapper
public interface TaskDaoXml {


    List<UserWorkVO> userWorkTop10(
                                    IPage<UserWorkVO> page,
                                    @Param("start") LocalDate startDate,
                                   @Param("end") LocalDate plusDays,
                                   @Param("isRepair") boolean isRepair,
                                   @Param("regionId") int regionId);
}
