package com.lkd.http.controller;
import com.lkd.entity.*;
import com.lkd.exception.LogicException;
import com.lkd.http.vo.*;
import com.lkd.service.*;
import com.lkd.utils.UserThreadLocal;
import com.lkd.vo.Pager;
import com.lkd.vo.UserWorkVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/task")
public class TaskController extends  BaseController{
    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskDetailsService taskDetailsService;

    @Autowired
    private TaskTypeService taskTypeService;



    /**
     * 搜索工单
     * @param pageIndex
     * @param pageSize
     * @param innerCode 设备编号
     * @param userId  工单所属人Id
     * @param taskCode 工单编号
     * @param status 工单状态
     * @param isRepair 是否是维修工单
     * @return
     */
    @GetMapping("/search")
    public Pager<TaskEntity> search(
            @RequestParam(value = "pageIndex",required = false,defaultValue = "1") Long pageIndex,
            @RequestParam(value = "pageSize",required = false,defaultValue = "10") Long pageSize,
            @RequestParam(value = "innerCode",required = false,defaultValue = "") String innerCode,
            @RequestParam(value = "userId",required = false,defaultValue = "") Integer userId,
            @RequestParam(value = "taskCode",required = false,defaultValue = "") String taskCode,
            @RequestParam(value = "status",required = false,defaultValue = "") Integer status,
            @RequestParam(value = "isRepair",required = false,defaultValue = "") Boolean isRepair,
            @RequestParam(value = "start",required = false,defaultValue = "") String start,
            @RequestParam(value = "end",required = false,defaultValue = "") String end){
        return taskService.search(pageIndex,pageSize,innerCode,userId,taskCode,status,isRepair,start,end);
    }



    /**
     * 根据taskId查询
     * @param taskId
     * @return 实体
     */
    @GetMapping("/taskInfo/{taskId}")
    public TaskEntity findById(@PathVariable Long taskId){
        return taskService.getById(taskId);
    }


    @GetMapping("/allTaskStatus")
    public List<TaskStatusTypeEntity> getAllStatus(){
        return taskService.getAllStatus();
    }

    /**
     * 获取工单类型
     * @return
     */
    @GetMapping("/typeList")
    public List<TaskTypeEntity> getProductionTypeList(){
        return taskTypeService.list();
    }

    /**
     * 获取工单详情
     * @param taskId
     * @return
     */
    @GetMapping("/details/{taskId}")
    public List<TaskDetailsEntity> getDetail(@PathVariable long taskId){
        return taskDetailsService.getByTaskId(taskId);
    }


    /**
     * 创建工单
     * @param taskViewModel 参数对象
     * @return true 操作成功 false 操作失败
     */
    @PostMapping("/create")
    public boolean create(@RequestBody TaskViewModel taskViewModel){
        //1.参数校验
        if(StringUtils.isBlank(taskViewModel.getInnerCode())){
            return false;
        }
        //2.执行业务逻辑
        //Integer assinorId = getUserId();
        Integer id = UserThreadLocal.userId.get();
        taskViewModel.setAssignorId(id);

        return taskService.create(taskViewModel);
        //3.返回结果
    }

    @GetMapping("/accept/{taskId}")
    public boolean accept(@PathVariable("taskId") Long taskId){

        taskService.accept(taskId);
        return true;
    }

    @PostMapping("/cancel/{taskId}")
    public boolean cancel(@PathVariable("taskId") Long taskId,
                       @RequestBody CancelTaskViewModel vo){

        taskService.cancel(taskId,vo);
        return true;
    }

    @GetMapping("/complete/{taskId}")
    public boolean complete(@PathVariable("taskId") Long taskId){

        taskService.complete(taskId);
        return true;
    }

    @GetMapping("/taskReportInfo/{start}/{end}")
    public List<TaskReportInfoVO> taskReportinfo(@PathVariable("start") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime start,
                               @PathVariable("end") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime end){
        return taskService.taskReportinfo(start,end);
    }

    //年月日
    @GetMapping("/userWorkTop10/{start}/{end}/{isRepair}/{regionId}")
    public List<UserWorkVO> test2(@PathVariable("start") String start,
                                  @PathVariable("end") String end,
                                  @PathVariable("isRepair") boolean isRepair,
                                  @PathVariable("regionId") int regionId) {
        LocalDate startDate = LocalDate.parse(start, DateTimeFormatter.ISO_DATE);
        LocalDate startEnd = LocalDate.parse(end, DateTimeFormatter.ISO_DATE);
        return taskService.userWorkTop10(startDate,startEnd,isRepair,regionId);
    }

    //年月日
    @GetMapping("/collectReport/{start}/{end}")
    public List<TaskCollectEntity> collectReport(@PathVariable("start")  String start,
                                                 @PathVariable("end")String end){
        LocalDate startDate = LocalDate.parse(start, DateTimeFormatter.ISO_DATE);
        LocalDate endDate = LocalDate.parse(end, DateTimeFormatter.ISO_DATE);
        return taskService.collectReport(startDate,endDate);
    }

    public static void main(String[] args) {
        LocalDate now = LocalDate.now();
        now.datesUntil(now.plusDays(5), Period.ofDays(1)).forEach(
                x -> {
                    System.out.println(x);
                }
        );
    }
}