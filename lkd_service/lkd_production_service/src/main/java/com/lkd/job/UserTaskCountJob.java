package com.lkd.job;

import com.lkd.common.VMSystem;
import com.lkd.dao.TaskDao;
import com.lkd.entity.UserCountEntity;
import com.lkd.feign.UserService;
import com.lkd.service.TaskService;
import com.lkd.vo.UserVO;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author brianxia
 * @version 1.0
 * @date 2023-02-10 10:36
 */
@Component
public class UserTaskCountJob {

    @Autowired
    private UserService userService;
    @Autowired
    private TaskDao taskDao;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private TaskService taskService;

    @XxlJob("userTaskCount")
    public void userTaskCount(){
        //1.获取所有运营和运维人员
        List<UserVO> allOperators = userService.getAllOperators(); //运营人员
        List<UserVO> allRepairers = userService.getAllRepairers(); //运维人员

        //2.获取所有工单量
        List<UserCountEntity> userIdTaskCount = taskDao.getUserIdTaskCount();
        Map<Integer, Long> countMap =
                userIdTaskCount.stream().collect(Collectors.toMap(UserCountEntity::getUserId, UserCountEntity::getCount));

        //3.组合数据，写入redis zset封装数据 value score
        List<UserVO> list = new ArrayList<>();
        list.addAll(allOperators);
        list.addAll(allRepairers);

        Set<String> keys = new HashSet<>();
        //合称一个所有人员集合
        for (UserVO userVO : list) {
            //使用redis的ZSet，key的规则，以固定字符串（前缀）VMSystem.REGION_TASK_KEY_PREF+日期+区域+人员角色（运营/运维）为key，
            // 以人员id做为值，
            // 以工单数作为分数(如果未完成工单中存在，就设置为未完成工单数，如果不存在，就设置为0)  过期时间为1天

            String key = VMSystem.generateTaskCountRedisKey(userVO.getRoleCode(), userVO.getRegionId());
            redisTemplate.opsForZSet().add(key
                    ,userVO.getUserId(),
                    countMap.containsKey(userVO.getUserId()) ?
                    countMap.get(userVO.getUserId()) : 0);

            keys.add(key);
        }

        keys.forEach(key -> {
            //设置过期时间为1天
            //redisTemplate.expire(key,1, TimeUnit.DAYS);
            redisTemplate.expire(key, Duration.ofDays(1));
        });

    }

    public static void main(String[] args) {
//        int t = 0;
//
//        int count = 100000;
//
//        ArrayList<Integer> list1 = new ArrayList<>();
//        for (int i = 0; i < count; i++) {
//            list1.add(i);
//        }
//
//        Map<Integer,Integer> map = new HashMap<>();
//        for (int i = 0; i < count; i++) {
//            map.put(i,i);
//        }
//
//
//        Date start = new Date();
//        for (Integer integer : list1) {
//            Integer integer1 = map.get(integer);
//            t++;
//        }
//        Date end = new Date();
//        System.out.println(end.getTime() - start.getTime());

        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("zhangsan","1");
        System.out.println(stringStringHashMap.get("lisi"));
        System.out.println(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
    }
}
