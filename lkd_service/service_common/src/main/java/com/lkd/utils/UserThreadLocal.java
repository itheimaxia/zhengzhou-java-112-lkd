package com.lkd.utils;

/**
 * @author brianxia
 * @version 1.0
 * @date 2023-02-07 16:09
 */
public class UserThreadLocal {
    public static final ThreadLocal<Integer> userId = new ThreadLocal<>();
}
