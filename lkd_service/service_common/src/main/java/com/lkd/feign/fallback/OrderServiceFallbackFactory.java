package com.lkd.feign.fallback;

import com.lkd.feign.OrderService;
import com.lkd.feign.UserService;
import com.lkd.vo.PartnerVO;
import com.lkd.vo.PayVO;
import com.lkd.vo.UserVO;
import com.lkd.vo.VmVO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class OrderServiceFallbackFactory implements FallbackFactory<OrderService> {
    @Override
    public OrderService create(Throwable throwable) {
        log.error("订单服务调用失败",throwable);
        return new OrderService() {

            @Override
            public Map requestPay(PayVO payVO) {
                return null;
            }
        };
    }
}
