package com.lkd.feign;

import com.lkd.feign.fallback.VmServiceFallbackFactory;
import com.lkd.vo.PayVO;
import com.lkd.vo.SkuVO;
import com.lkd.vo.VmVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@FeignClient(value = "order-service",fallbackFactory = VmServiceFallbackFactory.class)
public interface OrderService {

    @PostMapping("/order/requestPay")
    public Map requestPay(@RequestBody PayVO payVO);
}
